## Local Development Setup

For the step to set up the local development environment, please refer to the README in the main parent repository.

Do note that this application is dependent on the backend application `buzzstreet-backend-app` to run correctly.

After you have successfully started and run the environment (virtual machine) and the backend app, 
you can run this application with the steps and commands below.

## Installation

Add the `.env` file, these configuration are important for running the application on the local development environment.

```dotenv
## Application
BS_APP_API_URL=http://localhost:3001
```


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

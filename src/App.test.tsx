import React from 'react';
import { render, screen } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom'; // Import the Router
import App from './App';

test('renders the application title', () => {
  render(
      <Router>
        <App />
      </Router>
  );
  const titleElement = screen.getByText(/BuzzStreetSG/i);
  expect(titleElement).toBeInTheDocument();
});


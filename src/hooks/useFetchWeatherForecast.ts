
import useApi from './useApi';
import appConfig from '../config';
import WeatherForecastApiResponse from "../types/weather/WeatherForecastApiResponse";

const useFetchWeatherForecast = (selectedDate: Date | null, selectedLocation: string) => {

  const apiUrl = selectedDate && selectedLocation
      ? `${appConfig.BS_API_HOST}/weather?` +
        `dateTime=${selectedDate.toISOString()}&` +
        `location=${encodeURIComponent(selectedLocation)}`
      : '';

  const {
    data,
    isLoading,
    error
  } = useApi<WeatherForecastApiResponse>(apiUrl);

  return { data, isLoading, error };
};

export default useFetchWeatherForecast;

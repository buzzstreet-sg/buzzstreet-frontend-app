/**
 * useApi Custom Hook
 *
 * This hook is designed to abstract the API call mechanism. It handles fetching data from a given URL
 * with specified options and manages the loading and error states. It can be used for various HTTP methods
 * including GET, POST, PUT, and DELETE. The hook ensures that the fetching logic is encapsulated and reusable
 * across different components.
 *
 * Parameters:
 * - url (string): The URL endpoint for the API call.
 * - options (UseApiOptions): Optional configurations for the API call, including method, body, and headers.
 *
 * Returns:
 * - data (T | null): The data returned from the API, parameterized to be of type T.
 * - isLoading (boolean): Indicates whether the API call is currently in progress.
 * - error (string | null): Contains the error message if the API call fails, otherwise null.
 *
 * Usage:
 * const { data, isLoading, error } = useApi<ResponseType>('/api/data', { method: 'GET' });
 */

import { useState, useEffect, useMemo  } from 'react';

interface UseApiOptions {
  method?: 'GET' | 'POST' | 'PUT' | 'DELETE';
  body?: BodyInit | null;
  headers?: HeadersInit;
}

const useApi = <T,>(url: string, options?: UseApiOptions): {
  data: T | null,
  isLoading: boolean,
  error: string | null
} => {
  const [data, setData] = useState(null);
  const [isLoading, setLoading] = useState(false);
  const [error, setError] = useState<string | null>(null);

  const method = options?.method || 'GET';
  const body = options?.body || null;

  const headers = useMemo(() => {
    return {
      'Content-Type': 'application/json',
      ...options?.headers,
    };
  }, [options?.headers]);

  useEffect(() => {
    const fetchData = async () => {
      if (!url) return;

      setLoading(true);
      setError(null);

      try {
        const response = await fetch(url, { method, body, headers });

        const result = await response.json();

        if (result.status === 'success') {
          setData(result.data);
        } else {
          setError(result.message || 'Unexpected Error');
        }
      } catch (err) {
        const errMessage: string = (err instanceof Error) ? err.message : 'Unexpected Error';
        setError(errMessage);
      } finally {
        setLoading(false);
      }
    };

    fetchData();

  }, [url, method, body, headers]);

  return { data, isLoading, error };
};

export default useApi;

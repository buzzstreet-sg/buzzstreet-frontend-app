/**
 * Custom hook for managing the search API interactions.
 *
 * This hook constructs the URL and body for the search API based on the selected date and location,
 * and then uses the generic useApi hook to make the POST request.
 *
 * @param {Date | null} selectedDate - The selected date.
 * @param {string} selectedLocation - The selected location.
 * @returns An object containing response data, loading status, and any error from the API call.
 */
import useApi from './useApi';
import appConfig from '../config';

const useStoreSearchInBackend = (selectedDate: Date | null, selectedLocation: string) => {
  const searchApiUrl: string = selectedDate && selectedLocation
      ? `${appConfig.BS_API_HOST}/search`
      : '';

  const postBody = selectedDate && selectedLocation
      ? JSON.stringify({
        dateTime: selectedDate.toISOString(),
        location: selectedLocation,
      })
      : null;

  const { data, isLoading, error } = useApi(searchApiUrl, {
    method: 'POST',
    body: postBody,
  });

  return { data, isLoading, error };
};

export default useStoreSearchInBackend;

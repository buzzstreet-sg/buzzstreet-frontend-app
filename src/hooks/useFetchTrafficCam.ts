
import useApi from './useApi';
import appConfig from '../config';
import TrafficApiResponse from "../types/traffic/TrafficApiResponse";

const useFetchTrafficCam = (selectedDate: Date | null, selectedLocation: string) => {

  const apiUrl = selectedDate && selectedLocation
      ? `${appConfig.BS_API_HOST}/traffic?` +
        `dateTime=${selectedDate.toISOString()}&` +
        `location=${encodeURIComponent(selectedLocation)}`
      : '';

  const {
    data,
    isLoading,
    error
  } = useApi<TrafficApiResponse>(apiUrl);

  return { data, isLoading, error };
};

export default useFetchTrafficCam;

import { updateLocalStorageList } from "../utils/localStorageUtils";

interface SearchHistoryItem {
  searchForLocation: string;
  searchForTimestamp: string;
}

/**
 * Custom hook for managing search history in local storage.
 */
const useStoreSearchInLocal = () => {
  const addSearchHistory = (location: string, date: Date | null) => {
    if (!date || !location) return;

    const newSearchHistory: SearchHistoryItem = {
      searchForLocation: location,
      searchForTimestamp: date.toISOString(),
    };

    updateLocalStorageList<SearchHistoryItem>('searchHistory', newSearchHistory, 5);
  };

  return { addSearchHistory };
};

export default useStoreSearchInLocal;
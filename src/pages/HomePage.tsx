/**
 * HomePage Component
 *
 * This component serves as the main page of the application. It combines several subcomponents
 * such as DateTimeSelector, LocationListSelector, WeatherForecast, and TrafficCam to provide a comprehensive
 * view for selecting date, time, and location, and viewing corresponding weather and traffic information.
 * It features a CollapsibleBanner for recommendations. The component uses two custom hooks: useStoreSearchInBackend
 * to record user searches in the backend and useStoreSearchInLocal to manage search history in local storage.
 *
 * State:
 * - selectedLocation: Stores the currently selected location.
 * - selectedDate: Stores the currently selected date.
 * - isBannerOpen: Manages the visibility of the CollapsibleBanner.
 * - locationApiUrl: URL for fetching location data. Updated on date selection confirmation.
 * - locations, isLoading, error: Data and state management for location data API calls.
 *
 * Functions:
 * - handleDateChange: Updates the selectedDate state.
 * - handleDateSelectionConfirm: Constructs API URL with selected date for location fetching.
 * - handleLocationSelect: Updates the selectedLocation state and caches the new search history in local storage.
 * - handleBannerToggle: Toggles the visibility of the CollapsibleBanner.
 * - handleRecommendationSelect: Updates state based on selected recommendation and triggers location data fetching.
 *
 * Usage:
 * <HomePage />
 */

import React, { useState } from 'react';
import appConfig from "../config";
import './HomePage.css';
import useApi from "../hooks/useApi";
import useStoreSearchInLocal from "../hooks/useStoreSearchInLocal";
import useStoreSearchInBackend from "../hooks/useStoreSearchInBackend";
import { parseDateFromLocaleString } from "../utils/dateUtils";
import DateTimeSelector from '../components/DateTimeSelector/DateTimeSelector';
import LocationListSelector from '../components/LocationListSelector/LocationListSelector';
import TrafficCam from '../components/TrafficCam/TrafficCam';
import WeatherForecast from "../components/WeatherForecast/WeatherForecast";
import SectionWrapper from "../components/SectionWrapper/SectionWrapper";
import CollapsibleBanner from "../components/CollapsibleBanner/CollapsibleBanner"
import Recommendation from "../components/Recommendation/Recommendation";

interface SearchHistory {
  searchForLocation: string;
  searchForTimestamp: string;
}

const HomePage = () => {
  const [selectedLocation, setSelectedLocation] = useState('');
  const [selectedDate, setSelectedDate] = useState<Date | null>(new Date());
  const [isBannerOpen, setIsBannerOpen] = useState(false);
  const [locationApiUrl, setLocationApiUrl] = useState('');

  // API: Retrieve list of locations for the datetime selected.
  // Note: locationApiUrl state is only updated when the user confirms the date-time selection.
  const {
    data : locations,
    isLoading,
    error
  } = useApi<string[]>(locationApiUrl);

  // API: Save user search transaction. Not concern about return response here.
  useStoreSearchInBackend(selectedDate, selectedLocation);

  // Store self search history to cache into local browser storage.
  const { addSearchHistory } = useStoreSearchInLocal();

  /**
   * Handles changes in the date selection.
   * @param {Date | null} date - The newly selected date.
   */
  const handleDateChange = (date: Date | null) => {
    setSelectedDate(date);
  };

  /**
   * Confirms the date selection and updates the API URL for fetching locations.
   */
  const handleDateSelectionConfirm = () => {
    if (!selectedDate) return;
    setLocationApiUrl(`${appConfig.BS_API_HOST}/locations?dateTime=${selectedDate.toISOString()}`)
  }

  /**
   * Handles the selection of a location.
   * Updates the selected location state and caches the new search history.
   * @param {string} location - The selected location.
   */
  const handleLocationSelect = (location: string) => {
    setSelectedLocation(location);
    addSearchHistory(location, selectedDate);
  };

  /**
   * Toggles the open state of the collapsible banner.
   */
  const handleBannerToggle = () => {
    setIsBannerOpen(!isBannerOpen);
  };

  /**
   * Handles the selection of a recommendation.
   * Sets the selected location and date based on the recommendation.
   * @param {SearchHistory} searchHistory - The search history item selected.
   */
  const handleRecommendationSelect = (searchHistory: SearchHistory ) => {
    // update selected location and date.
    setSelectedLocation(searchHistory.searchForLocation);
    setSelectedDate(parseDateFromLocaleString(searchHistory.searchForTimestamp));

    // Close the recommendation banner.
    setIsBannerOpen(false);

    // Check in date selection confirm to triger the fetch of locations to display..
    handleDateSelectionConfirm();
  };

  const recommendationContent = (
      <Recommendation onRecommendationSelect={ handleRecommendationSelect } />
  );

  return (
      <div className="mainContainer">
        <CollapsibleBanner
            title="Need Some Recomendation?"
            content={recommendationContent}
            isOpen={isBannerOpen}
            onToggle={handleBannerToggle}
        />

        <SectionWrapper title="Date and Time Selection">
          <DateTimeSelector
              selectedDate={selectedDate}
              onDateChange={handleDateChange}
              onConfirm={handleDateSelectionConfirm}/>
        </SectionWrapper>

        <SectionWrapper title="Location Selection">
          <LocationListSelector
              selectedLocation={selectedLocation}
              locations={locations || []}
              isLoading={isLoading}
              error={error}
              onLocationSelect={handleLocationSelect}
          />
        </SectionWrapper>

        <SectionWrapper title="2H Weather Forecast">
          <WeatherForecast selectedDate={selectedDate} selectedLocation={selectedLocation}/>
        </SectionWrapper>

        <SectionWrapper title="Traffic Camera View">
          <TrafficCam selectedDate={selectedDate} selectedLocation={selectedLocation}/>
        </SectionWrapper>
      </div>
  );
};

export default HomePage;


/**
 * parseDateFromLocaleString Function
 *
 * This function parses a date string in a specific locale format and returns
 * a JavaScript Date object. The expected format of the input string is "DD/MM/YYYY, hh:mm:ss am/pm".
 * This utility function is particularly useful in scenarios where date and time
 * are presented in a human-readable format and need to be converted into a Date object
 * for further processing or comparison.
 *
 * Parameters:
 * - dateStr (string): The date string to be parsed. Expected format: "DD/MM/YYYY, hh:mm:ss am/pm".
 *
 * Returns:
 * - Date: A JavaScript Date object representing the parsed date and time.
 *
 * Usage:
 * const date = parseDateFromLocaleString("31/12/2023, 11:59:59 pm");
 */
export const parseDateFromLocaleString = (dateStr: string): Date => {
  // Split the date and time
  const [datePart, timePart] = dateStr.split(', ');

  // Extract date components
  const [day, month, year] = datePart.split('/').map(num => parseInt(num, 10));

  // Extract time components
  const [time, modifier] = timePart.split(' ');
  let [hours, minutes, seconds] = time.split(':').map(num => parseInt(num, 10));

  // Adjust hours for 12-hour clock format
  if (modifier.toLowerCase() === 'pm' && hours < 12) {
    hours += 12;
  } else if (modifier.toLowerCase() === 'am' && hours === 12) {
    hours = 0;
  }

  // Return the new Date object
  return new Date(year, month - 1, day, hours, minutes, seconds);
};

/**
 * Updates a list in local storage. Adds a new item to the beginning of the list and trims its size.
 * @param {string} key - The local storage key under which the list is stored.
 * @param {T} newItem - The new item to add to the list.
 * @param {number} maxSize - The maximum size of the list.
 */
export const updateLocalStorageList = <T>(key: string, newItem: T, maxSize: number): void => {
  const existingListJsonStr = localStorage.getItem(key);
  const existingList: T[] = existingListJsonStr ? JSON.parse(existingListJsonStr) : [];

  existingList.unshift(newItem);
  const updatedList = existingList.slice(0, maxSize);
  localStorage.setItem(key, JSON.stringify(updatedList));
};

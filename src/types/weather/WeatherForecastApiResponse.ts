
interface WeatherForecastApiResponse {
  location: string,
  dateTime: string,
  forecast: string | null;
}

export default WeatherForecastApiResponse;

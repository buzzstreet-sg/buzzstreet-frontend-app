
interface TrafficApiResponse {
  location: string,
  dateTime: string,
  images: string[];
}

export default TrafficApiResponse;

/**
 * DateTimeSelector Component
 *
 * This component provides a user interface for selecting date and time. It is built using Material-UI's
 * DateTimePicker component. It includes a confirm button to submit the selected date and time, and a reset
 * button to set the date and time to the current moment. The component is styled using Material-UI's sx prop
 * and supports responsive design for smaller screens.
 *
 * Props:
 * - selectedDate (Date | null): The currently selected date and time.
 * - onDateChange ((date: Date | null) => void): Callback function called when the date is changed.
 * - onConfirm (() => void): Callback function called when the confirm button is clicked.
 *
 * Usage:
 * <DateTimeSelector
 *    selectedDate={selectedDate}
 *    onDateChange={handleDateChange}
 *    onConfirm={handleConfirm}
 * />
 */

import * as React from 'react';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import { Button } from '@mui/material';
import './DateTimeSelector.css';

interface DateTimeSelectorProps {
  selectedDate: Date | null;
  onDateChange: (date: Date | null) => void;
  onConfirm: () => void;
}

const DateTimeSelector = ({ selectedDate, onDateChange, onConfirm }: DateTimeSelectorProps) => {

  /**
   * Resets the selected date to the current date and time.
   */
  const handleReset = () => {
    const currentDate = new Date();
    onDateChange(currentDate);
  };

  return (
      <div className="dateTimeSelectorContainer">
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <DateTimePicker
              sx={{
                // Example styles
                width: '100%',
                '.MuiInputBase-input': { // Targeting the input field specifically
                  fontSize: '1.2rem',
                  textAlign: 'center',
                  color: '#333', // Change as needed
                },
                '.MuiSvgIcon-root': { // Targeting the calendar icon
                  color: '#666', // Change as needed
                },
                // Add more styles as needed
              }}
              value={selectedDate}
              onChange={onDateChange}
          />
          <div className="buttonGroup">
            <Button
                sx={{
                  flexGrow: 1,
                  margin: '5px',
                  color: 'white',
                  backgroundColor: '#2196F3',
                  '&:hover': {
                    backgroundColor: '#1E88E5',
                  },
                  // Media query for smaller screens
                  '@media (max-width: 800px)': {
                    width: '100%',
                    margin: '5px 0',
                    marginTop: '5px',
                  }
                }}
                variant="contained"
                onClick={onConfirm}
            >
              Confirm
            </Button>
            <Button
                sx={{
                  flexGrow: 1,
                  margin: '5px',
                  color: '#333',
                  borderColor: '#ccc',
                  backgroundColor: 'transparent',
                  '&:hover': {
                    borderColor: '#d32f2f',
                  },
                  // Media query for smaller screens
                  '@media (max-width: 800px)': {
                    width: '100%',
                    margin: '5px 0',
                    marginBottom: '5px',
                  }
                }}
                variant="outlined"
                onClick={handleReset}
            >
              Reset
            </Button>
          </div>
        </LocalizationProvider>
      </div>

  );
};

export default DateTimeSelector;

import React from 'react';
import { render, screen, fireEvent, within } from '@testing-library/react';
import DateTimeSelector from './DateTimeSelector';
import { format } from 'date-fns';

describe('DateTimeSelector Component', () => {
  const mockOnDateChange = jest.fn();
  const mockOnConfirm = jest.fn();
  const selectedDate = new Date(2022, 7, 1); // Example date: 1st August 2022

  beforeEach(() => {
    render(
        <DateTimeSelector
            selectedDate={selectedDate}
            onDateChange={mockOnDateChange}
            onConfirm={mockOnConfirm}
        />
    );
  });

  test('renders DateTimePicker with provided date', () => {
    // Check if the DateTimePicker is present
    const dateTimePicker = screen.getByRole('textbox');
    expect(dateTimePicker).toBeInTheDocument();
    // Additional checks for the displayed date can be added if needed
  });

  test('calls onDateChange when date is changed', () => {
    fireEvent.change(screen.getByRole('textbox'), { target: { value: 'new date value' } });
    expect(mockOnDateChange).toHaveBeenCalled();
  });

  test('calls onConfirm when confirm button is clicked', () => {
    fireEvent.click(screen.getByText(/Confirm/i));
    expect(mockOnConfirm).toHaveBeenCalledTimes(1);
  });

  test('resets date to current when reset button is clicked', () => {
    fireEvent.click(screen.getByText(/Reset/i));
    // Check if the date is reset to the current date
    // This may require mocking Date or adjusting expectations
    expect(mockOnDateChange).toHaveBeenCalledWith(expect.any(Date));
  });
});

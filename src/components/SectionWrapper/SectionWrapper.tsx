import React from 'react';
import './SectionWrapper.css'; // Import the CSS file
/**
 * SectionWrapper Component
 *
 * This component serves as a generic wrapper for various sections of an application.
 * It provides a consistent layout structure, including a title and an optional label,
 * and encapsulates any children components passed to it. The `SectionWrapper` is ideal
 * for grouping content logically and providing a uniform appearance across different
 * sections of an application.
 *
 * Props:
 * - title (string): The title text for the section. Displayed prominently at the top of the section.
 * - label (React.ReactNode, optional): An optional label element that can be displayed alongside the title.
 * - children (React.ReactNode): The content to be wrapped within the section. This can include any React components or elements.
 *
 * Usage:
 * <SectionWrapper title="My Section" label={<SomeLabelComponent />}>
 *   <p>Content goes here</p>
 * </SectionWrapper>
 */


interface SectionWrapperProps {
  title: string;
  label?: React.ReactNode;
  children: React.ReactNode;
}

const SectionWrapper: React.FC<SectionWrapperProps> = ({ title, label, children }) => {
  return (
      <div className="sectionWrapper">
        <div className="sectionTitle">{title}</div>
        {label && <div className="sectionLabel">{label}</div>}
        <div className="sectionContent">{children}</div>
      </div>
  );
};

export default SectionWrapper;

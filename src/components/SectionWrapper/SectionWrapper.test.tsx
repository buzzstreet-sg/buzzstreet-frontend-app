import React from 'react';
import { render, screen } from '@testing-library/react';
import SectionWrapper from './SectionWrapper';

describe('SectionWrapper Component', () => {
  const title = 'Test Title';
  const label = <span>Test Label</span>;
  const childrenContent = <p>Test Children</p>;

  test('renders with provided title', () => {
    render(<SectionWrapper title={title}>{childrenContent}</SectionWrapper>);
    expect(screen.getByText(title)).toBeInTheDocument();
  });

  test('renders with provided label', () => {
    render(
        <SectionWrapper title={title} label={label}>
          {childrenContent}
        </SectionWrapper>
    );
    expect(screen.getByText('Test Label')).toBeInTheDocument();
  });

  test('does not render label when not provided', () => {
    render(<SectionWrapper title={title}>{childrenContent}</SectionWrapper>);
    expect(screen.queryByText('Test Label')).toBeNull();
  });

  test('renders children content', () => {
    render(<SectionWrapper title={title}>{childrenContent}</SectionWrapper>);
    expect(screen.getByText('Test Children')).toBeInTheDocument();
  });
});

/**
 * LocationListSelector Component
 *
 * This component provides an interface for displaying a list of locations and selecting one from the list.
 * It displays the locations as a list, where each item can be clicked to select it. The component handles
 * different states including loading, error, and empty list scenarios, providing appropriate feedback
 * or placeholder content in each case. This component is ideal for scenarios where users need to choose a
 * location from a predefined list.
 *
 * Props:
 * - selectedLocation (string): The currently selected location.
 * - locations (string[]): An array of location strings to be displayed as list items.
 * - isLoading (boolean): Indicates whether the location data is being loaded.
 * - error (string | null): Contains the error message if there is an issue fetching the locations.
 * - onLocationSelect ((location: string) => void): Callback function called when a location is selected.
 *
 * Usage:
 * <LocationListSelector
 *    selectedLocation={selectedLocation}
 *    locations={locationArray}
 *    isLoading={isLoadingState}
 *    error={errorState}
 *    onLocationSelect={handleLocationSelect}
 * />
 */

import React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import './LocationListSelector.css';
import Placeholder from "../Placeholder/Placeholder";

interface LocationListSelectorProps {
  selectedLocation: string;
  locations: string[];
  isLoading: boolean;
  error: string | null;
  onLocationSelect: (location: string) => void;
}

const LocationListSelector = ({ selectedLocation, locations, isLoading, error, onLocationSelect }: LocationListSelectorProps) => {

  if (isLoading) {
    return <Placeholder message="Loading locations..." isLoading={true} />
  }

  if (error) {
    return <Placeholder message="Sorry, something went wrong. Please try again later." icon="📍" />
  }

  if (locations.length === 0) {
    return <Placeholder message="No location data. Select data and time." icon="📍" />
  }

  return (
      <div className="locationListSelectorContainer">
        <List>
          {locations.map((location, index) => (
              <ListItem
                  key={index}
                  className={`locationListItem ${location === selectedLocation ? 'selected' : ''}`}
                  onClick={() => onLocationSelect(location)}
              >
                <ListItemButton>
                  {location}
                </ListItemButton>
              </ListItem>
          ))}
        </List>
      </div>
  );
};

export default LocationListSelector;

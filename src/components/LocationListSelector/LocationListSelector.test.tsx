import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import LocationListSelector from './LocationListSelector';

describe('LocationListSelector Component', () => {
  const mockOnLocationSelect = jest.fn();
  const locations = ['Location 1', 'Location 2', 'Location 3'];

  test('renders location list', () => {
    render(
        <LocationListSelector
            selectedLocation=""
            locations={locations}
            isLoading={false}
            error={null}
            onLocationSelect={mockOnLocationSelect}
        />
    );

    // Check if all locations are rendered
    locations.forEach(location => {
      expect(screen.getByText(location)).toBeInTheDocument();
    });
  });

  test('handles location selection', () => {
    render(
        <LocationListSelector
            selectedLocation=""
            locations={locations}
            isLoading={false}
            error={null}
            onLocationSelect={mockOnLocationSelect}
        />
    );

    // Simulate selecting a location
    fireEvent.click(screen.getByText('Location 1'));
    expect(mockOnLocationSelect).toHaveBeenCalledWith('Location 1');
  });

  test('renders loading state', () => {
    render(
        <LocationListSelector
            selectedLocation=""
            locations={[]}
            isLoading={true}
            error={null}
            onLocationSelect={mockOnLocationSelect}
        />
    );
    expect(screen.getByText('Loading locations...')).toBeInTheDocument();
  });

  test('renders error state', () => {
    render(
        <LocationListSelector
            selectedLocation=""
            locations={[]}
            isLoading={false}
            error="Error fetching locations"
            onLocationSelect={mockOnLocationSelect}
        />
    );
    expect(screen.getByText('Sorry, something went wrong. Please try again later.')).toBeInTheDocument();
  });

  test('renders empty state', () => {
    render(
        <LocationListSelector
            selectedLocation=""
            locations={[]}
            isLoading={false}
            error={null}
            onLocationSelect={mockOnLocationSelect}
        />
    );
    expect(screen.getByText('No location data. Select data and time.')).toBeInTheDocument();
  });
});

/**
 * Recommendation Component
 *
 * This component is designed to display recommendations based on previous search histories,
 * both from the current user and from other users. It retrieves and processes the user's search
 * history from local storage and also fetches recent searches from other users via an API call.
 * The component presents these searches in a list format, allowing users to select and revisit
 * past search criteria. This is useful for applications with features like location and time-based
 * searches, where revisiting previous choices can enhance the user experience.
 *
 * Props:
 * - onRecommendationSelect ((selectedSearchHistory: SearchHistory) => void):
 *   A callback function that is called when a user selects a recommendation.
 *   It receives the selected search history item.
 *
 * Usage:
 * <Recommendation onRecommendationSelect={handleRecommendationSelect} />
 */


import React, { useEffect, useState } from 'react';
import useApi from "../../hooks/useApi";
import appConfig from "../../config";
import SimpleList from "../SimpleList/SimpleList";
import SectionWrapper from "../SectionWrapper/SectionWrapper";
import './Recommendation.css';

interface SearchHistory {
  searchForLocation: string;
  searchForTimestamp: string;
}

interface RecommendationProps {
  onRecommendationSelect: (selectedSearchHistory: SearchHistory) => void;
}

const Recommendation: React.FC<RecommendationProps> = ({ onRecommendationSelect }) => {
  const [previousSearches, setPreviousSearches] = useState<SearchHistory[]>([]);
  const [isLoadingPreviousSearches, setIsLoadingPreviousSearches] = useState(true);

  // Fetch user's previous searches.
  useEffect(() => {
    const cachedSearches = JSON.parse(localStorage.getItem('searchHistory') || '[]');
    setPreviousSearches(cachedSearches.slice(0, 5));
    setIsLoadingPreviousSearches(false);
  }, []);

  // Process previous searches for display.
  const processedSelfSearches = previousSearches
      ? previousSearches.slice(0, 5).map(item =>
          `${new Date(item.searchForTimestamp).toLocaleString()} - ${item.searchForLocation}`
      )
      : [];

  // API: Retrieve other people's searches from the backend.
  const {
    data: otherSearches,
    isLoading: isLoadingOtherSearches
  } = useApi<SearchHistory[]>(`${appConfig.BS_API_HOST}/search/recent`);

  // Process other people's searches for display
  const processedOtherSearches = otherSearches
      ? otherSearches.slice(0, 5).map(item =>
          `${new Date(item.searchForTimestamp).toLocaleString()} - ${item.searchForLocation}`
      )
      : [];

  const handleItemSelect = (selectedItem: string) => {
    // Split the string by " - ", which was used to format the string
    const [searchForTimestamp, searchForLocation] = selectedItem.split(" - ");

    // Call the callback with an object containing the extracted data
    onRecommendationSelect({ searchForLocation, searchForTimestamp });
  };

  return (
      <div className="recommendationContainer">
        <div className="recommendationListWrapper">
          <SectionWrapper title="Other People's Searches">
            <SimpleList
                items={processedOtherSearches}
                isLoading={isLoadingOtherSearches}
                onItemSelect={handleItemSelect}
            />
          </SectionWrapper>
        </div>

        <div className="recommendationListWrapper">
          <SectionWrapper title="Your Previous Searches">
            <SimpleList
                items={processedSelfSearches}
                isLoading={isLoadingPreviousSearches}
                onItemSelect={handleItemSelect}
            />
          </SectionWrapper>
        </div>
      </div>
  );
};

export default Recommendation;

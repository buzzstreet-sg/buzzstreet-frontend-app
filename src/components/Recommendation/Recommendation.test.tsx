import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import Recommendation from './Recommendation';
import useApi from '../../hooks/useApi';

// Mocking useApi and localStorage
jest.mock('../../hooks/useApi');

type MockLocalStorage = {
  [key: string]: string;
};

const mockLocalStorage: MockLocalStorage = {};

const mockLocalStorageObject = {
  getItem: jest.fn((key: string) => mockLocalStorage[key] || null),
  setItem: jest.fn((key: string, value: string) => { mockLocalStorage[key] = value; }),
  clear: jest.fn(() => { Object.keys(mockLocalStorage).forEach(key => delete mockLocalStorage[key]); }),
};

// Apply the mock to the window object
Object.defineProperty(window, 'localStorage', {
  value: mockLocalStorageObject,
  writable: true
});

describe('Recommendation Component', () => {
  const mockOnRecommendationSelect = jest.fn();
  const mockPreviousSearches = [
    { searchForLocation: 'Location 1', searchForTimestamp: '2021-01-01T00:00:00.000Z' },
    // ... other mock searches ...
  ];
  const mockOtherSearches = [
    { searchForLocation: 'Location 2', searchForTimestamp: '2021-01-02T00:00:00.000Z' },
    // ... other mock searches ...
  ];

  beforeEach(() => {
    // Resetting the mock functions
    mockLocalStorageObject.getItem.mockReset();
    mockLocalStorageObject.setItem.mockReset();
    mockLocalStorageObject.clear.mockReset();

    // Setting up mock return values
    mockLocalStorageObject.getItem.mockReturnValue(JSON.stringify(mockPreviousSearches));
    (useApi as jest.Mock).mockReturnValue({ data: mockOtherSearches, isLoading: false });
  });

  test('renders previous and other people\'s searches', () => {
    render(<Recommendation onRecommendationSelect={mockOnRecommendationSelect} />);
    expect(screen.getByText(/Location 1/i)).toBeInTheDocument();
    expect(screen.getByText(/Location 2/i)).toBeInTheDocument();
  });

  test('calls onRecommendationSelect when an item is selected', () => {
    render(<Recommendation onRecommendationSelect={mockOnRecommendationSelect} />);

    // Simulate clicking on an item
    fireEvent.click(screen.getByText(/Location 1/i));

    // Convert the expected date to the localized string format
    const expectedDate = new Date("2021-01-01T00:00:00.000Z").toLocaleString();

    // Check if the mock function was called with the correct arguments
    expect(mockOnRecommendationSelect).toHaveBeenCalledWith({
      searchForLocation: 'Location 1',
      searchForTimestamp: expectedDate,
    });
  });

  test('handles loading state for other people\'s searches', () => {
    (useApi as jest.Mock).mockReturnValue({ data: null, isLoading: true });
    render(<Recommendation onRecommendationSelect={mockOnRecommendationSelect} />);
    expect(screen.getByText(/Loading searches.../i)).toBeInTheDocument();
  });
});

import React from 'react';
import { render, screen } from '@testing-library/react';
import WeatherForecast from './WeatherForecast';
import useFetchWeatherForecast from '../../hooks/useFetchWeatherForecast';

// Mock the custom hook
jest.mock('../../hooks/useFetchWeatherForecast');

describe('WeatherForecast Component', () => {
  const selectedDate = new Date();
  const selectedLocation = 'Test Location';
  const mockForecast = 'Sunny';

  beforeEach(() => {
    // Clear all mocks before each test
    jest.clearAllMocks();
  });

  test('renders loading state', () => {
    (useFetchWeatherForecast as jest.Mock).mockReturnValue({ isLoading: true, data: null, error: null });
    render(<WeatherForecast selectedDate={selectedDate} selectedLocation={selectedLocation} />);
    expect(screen.getByText('Loading weather forecast data...')).toBeInTheDocument();
  });

  test('renders error state', () => {
    (useFetchWeatherForecast as jest.Mock).mockReturnValue({ isLoading: false, data: null, error: 'Error message' });
    render(<WeatherForecast selectedDate={selectedDate} selectedLocation={selectedLocation} />);
    expect(screen.getByText(/Sorry, something went wrong. Please try again later./i)).toBeInTheDocument();
  });

  test('renders no data state when no forecast is available', () => {
    (useFetchWeatherForecast as jest.Mock).mockReturnValue({ isLoading: false, data: { forecast: '' }, error: null });
    render(<WeatherForecast selectedDate={selectedDate} selectedLocation={selectedLocation} />);
    expect(screen.getByText('No weather data.')).toBeInTheDocument();
  });

  test('renders forecast data when available', () => {
    (useFetchWeatherForecast as jest.Mock).mockReturnValue({ isLoading: false, data: { forecast: mockForecast }, error: null });
    render(<WeatherForecast selectedDate={selectedDate} selectedLocation={selectedLocation} />);
    expect(screen.getByText(mockForecast)).toBeInTheDocument();
  });

  test('renders no data state when no forecast is available', () => {
    (useFetchWeatherForecast as jest.Mock).mockReturnValue({ isLoading: false, data: {}, error: null });
    render(<WeatherForecast selectedDate={selectedDate} selectedLocation={selectedLocation} />);
    expect(screen.getByText(/No weather data./i)).toBeInTheDocument();
  });
});

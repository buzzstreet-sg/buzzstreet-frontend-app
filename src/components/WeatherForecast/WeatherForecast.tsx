/**
 * WeatherForecast Component
 *
 * This component is designed to display the weather forecast based on a selected date and location.
 * It fetches weather forecast data from an API and presents it in a readable format. The component
 * handles various states including loading, error, and no data scenarios, providing appropriate
 * feedback or placeholder content for each. This component is ideal for applications that require
 * weather data visualization for planning or informational purposes.
 *
 * Props:
 * - selectedDate (Date | null): The date for which weather data is to be fetched.
 * - selectedLocation (string): The location for which weather data is to be fetched.
 *
 * Usage:
 * <WeatherForecast
 *    selectedDate={selectedDate}
 *    selectedLocation={selectedLocation}
 * />
 */
import React, { useState, useEffect } from 'react';
import { Card, Typography } from '@mui/material';
import './WeatherForecast.css';
import useFetchWeatherForecast from "../../hooks/useFetchWeatherForecast";
import Placeholder from "../Placeholder/Placeholder";

interface WeatherForecastProps {
  selectedDate: Date | null;
  selectedLocation: string;
}

const WeatherForecast = ({ selectedDate, selectedLocation }: WeatherForecastProps) => {
  const [forecast, setForecast] = useState('');

  // API: Retrieve weather forecast data based on the selected datetime and location.
  const {
    data,
    isLoading,
    error
  } = useFetchWeatherForecast(selectedDate, selectedLocation);

  // Store the forecast information for display.
  useEffect(() => {
    setForecast(data?.forecast || '');
  }, [data]);

  if (isLoading) {
    return <
      Placeholder
        message="Loading weather forecast data..."
        isLoading={true}
    />
  }

  if (!selectedDate || !selectedLocation) {
    return <
        Placeholder
        message="No date or location selected. Please select the date and location."
        icon="☀️"
    />;
  }

  if (!selectedDate) {
    return <
      Placeholder
        message="No weather data. Please select the date, time and location"
        icon="☀️"
    />
  }

  if (error) {
    return <
        Placeholder
        message="Sorry, something went wrong. Please try again later."
        icon="☀️"
    />;
  }

  if (!forecast) {
    return <
      Placeholder
        message="No weather data."
        icon="☀️"
    />
  }

  return (
      <Card className="weatherForecastCard">
        <Typography variant="body1" className="weatherText">
          <span className="weatherIcon">☀️</span>
          {forecast}
        </Typography>
      </Card>
  );
};

export default WeatherForecast;

import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import CollapsibleBanner from './CollapsibleBanner';

describe('CollapsibleBanner Component', () => {
  const title = 'Test Title';
  const content = <div>Test Content</div>;

  test('renders with title', () => {
    render(<CollapsibleBanner title={title} content={content} isOpen={false} onToggle={() => {}} />);
    expect(screen.getByText(title)).toBeInTheDocument();
  });

  test('renders content when open', () => {
    render(<CollapsibleBanner title={title} content={content} isOpen={true} onToggle={() => {}} />);
    expect(screen.getByText('Test Content')).toBeInTheDocument();
  });

  test('does not render content when closed', () => {
    render(<CollapsibleBanner title={title} content={content} isOpen={false} onToggle={() => {}} />);
    expect(screen.queryByText('Test Content')).toBeNull();
  });

  test('toggle button changes state', () => {
    const handleToggle = jest.fn();
    render(<CollapsibleBanner title={title} content={content} isOpen={false} onToggle={handleToggle} />);

    const toggleButton = screen.getByRole('button');
    fireEvent.click(toggleButton);

    expect(handleToggle).toHaveBeenCalledTimes(1);
  });
});

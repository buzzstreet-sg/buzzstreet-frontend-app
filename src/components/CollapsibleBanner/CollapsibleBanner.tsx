/**
 * CollapsibleBanner Component
 *
 * This component provides a collapsible banner functionality, which can be used to show or hide content
 * dynamically in a UI. It is structured with a header section that contains a title and an icon button
 * to control the collapse action. The content section expands or collapses based on the `isOpen` prop.
 *
 * Props:
 * - title (string): The title text to be displayed in the banner header.
 * - content (React.ReactNode): The content to be displayed within the collapsible section of the banner.
 * - isOpen (boolean): A boolean prop to control the open/close state of the collapsible content.
 * - onToggle (() => void): A callback function that is called when the header is clicked, intended to toggle the open/close state.
 *
 * Usage:
 * <CollapsibleBanner
 *    title="Example Title"
 *    content={<div>Content here</div>}
 *    isOpen={isOpenState}
 *    onToggle={handleToggle}
 * />
 */

import React from 'react';
import { Collapse, Typography, IconButton } from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import './CollapsibleBanner.css';

type CollapsibleBannerProps = {
  title: string;
  content: React.ReactNode;
  isOpen: boolean; // Prop to control open/close state
  onToggle: () => void; // Callback to toggle the state
};

const CollapsibleBanner: React.FC<CollapsibleBannerProps> = ({title, content, isOpen, onToggle }) => {

  return (
      <div className="collapsibleBanner">
        <div className="collapsibleBannerHeader" onClick={onToggle}>
          {/* Title and toggle button/icon */}
          <Typography variant="h6">{title}</Typography>
          <IconButton>
            <ExpandMoreIcon style={{transform: isOpen ? 'rotate(180deg)' : 'none'}}/>
          </IconButton>
        </div>
        <Collapse in={isOpen}>
          {isOpen && (
              <div className="collapsibleBannerContent">
                {content}
              </div>
          )}
        </Collapse>
      </div>
  );
};

export default CollapsibleBanner;

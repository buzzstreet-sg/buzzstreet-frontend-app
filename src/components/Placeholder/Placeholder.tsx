/**
 * Placeholder Component
 *
 * This component serves as a general-purpose placeholder to be used across the application.
 * It displays a message to the user, optionally accompanied by an icon or a loading spinner.
 * The Placeholder component is useful for indicating loading states, empty states, or providing
 * general information or feedback. The component is styled to be visually consistent and attention-grabbing.
 *
 * Props:
 * - message (string): The text message to be displayed in the placeholder.
 * - icon (string, optional): An optional string representing an icon, displayed next to the message. Defaults to a document icon.
 * - isLoading (boolean, optional): If true, shows a loading spinner instead of the icon. Defaults to false.
 *
 * Usage:
 * <Placeholder
 *    message="Loading data..."
 *    isLoading={true}
 * />
 *
 * <Placeholder
 *    message="No data available."
 *    icon="🚫"
 * />
 */

import React from 'react';
import { CircularProgress, Box } from '@mui/material';
import './Placeholder.css';

interface PlaceholderProps {
  message: string;
  icon?: string;       // Optional icon prop
  isLoading?: boolean; // Optional loading state prop
}

const Placeholder: React.FC<PlaceholderProps> = ({ message, icon = '📄', isLoading = false }) => {
  return (
      <Box className="placeholder">
        {isLoading ? (
            <CircularProgress />
        ) : (
            <span className="placeholderIcon">{icon}</span>
        )}
        <p className="placeholderText">{message}</p>
      </Box>
  );
};

export default Placeholder;

import React from 'react';
import { render, screen } from '@testing-library/react';
import Placeholder from './Placeholder';
import { CircularProgress } from '@mui/material';

describe('Placeholder Component', () => {
  const message = 'Test Message';

  test('renders with provided message', () => {
    render(<Placeholder message={message} />);
    expect(screen.getByText(message)).toBeInTheDocument();
  });

  test('shows loading spinner when isLoading is true', () => {
    render(<Placeholder message={message} isLoading={true} />);
    expect(screen.getByRole('progressbar')).toBeInTheDocument();
    expect(screen.queryByText('📄')).toBeNull(); // Default icon should not be visible
  });

  test('shows default icon when no icon is provided and not loading', () => {
    render(<Placeholder message={message} />);
    expect(screen.getByText('📄')).toBeInTheDocument(); // Default icon
  });

  test('shows provided icon when specified and not loading', () => {
    const customIcon = '🚫';
    render(<Placeholder message={message} icon={customIcon} />);
    expect(screen.getByText(customIcon)).toBeInTheDocument();
  });
});

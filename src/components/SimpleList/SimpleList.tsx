/**
 * SimpleList Component
 *
 * This component is designed to render a list of items, providing an interactive
 * and straightforward list UI. It allows users to select an item from the list,
 * and it can display a loading state or a placeholder message when there are no
 * items to show. The `SimpleList` component is useful in various scenarios where
 * a simple, interactive list is required, such as displaying search results,
 * user selections, or other lists of text-based items.
 *
 * Props:
 * - items (string[]): An array of strings representing the list items to be displayed.
 * - isLoading (boolean): Indicates whether the list is in a loading state.
 *                        If true, a loading placeholder is shown.
 * - onItemSelect ((item: string) => void): Callback function that is called when
 *                                          an item in the list is selected.
 *
 * Usage:
 * <SimpleList
 *    items={['Item 1', 'Item 2', 'Item 3']}
 *    isLoading={false}
 *    onItemSelect={handleItemSelection}
 * />
 */

import React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import Placeholder from "../Placeholder/Placeholder";
import './SimpleList.css';

interface SimpleListProps {
  items: string[];
  isLoading: boolean;
  onItemSelect: (item: string) => void;
}

const SimpleList: React.FC<SimpleListProps> = ({ items, isLoading, onItemSelect }) => {
  const [selectedItem, setSelectedItem] = React.useState<string | null>(null);

  const handleItemSelect = (item: string) => {
    setSelectedItem(item);
    onItemSelect(item);
  };

  if (isLoading) {
    return <Placeholder message="Loading searches..." isLoading={true} />
  }

  if (!items || items.length === 0) {
    return <Placeholder message="No Data Found" icon="📍" />
  }

  return (
      <div className="listContainer">
        <List>
          {items.map((item, index) => (
              <ListItem
                  key={index}
                  className={`listItem ${item === selectedItem ? 'selected' : ''}`}
                  onClick={() => handleItemSelect(item)}
              >
                <ListItemButton>
                  {item}
                </ListItemButton>
              </ListItem>
          ))}
        </List>
      </div>
  );
};

export default SimpleList;

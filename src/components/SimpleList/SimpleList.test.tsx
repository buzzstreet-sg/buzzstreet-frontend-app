import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import SimpleList from './SimpleList';

describe('SimpleList Component', () => {
  const items = ['Item 1', 'Item 2', 'Item 3'];
  const mockOnItemSelect = jest.fn();

  test('renders list items', () => {
    render(<SimpleList items={items} isLoading={false} onItemSelect={mockOnItemSelect} />);
    items.forEach(item => {
      expect(screen.getByText(item)).toBeInTheDocument();
    });
  });

  test('handles item selection', () => {
    render(<SimpleList items={items} isLoading={false} onItemSelect={mockOnItemSelect} />);
    fireEvent.click(screen.getByText('Item 1'));
    expect(mockOnItemSelect).toHaveBeenCalledWith('Item 1');
  });

  test('renders loading state', () => {
    render(<SimpleList items={[]} isLoading={true} onItemSelect={mockOnItemSelect} />);
    expect(screen.getByText('Loading searches...')).toBeInTheDocument();
  });

  test('renders placeholder when no items', () => {
    render(<SimpleList items={[]} isLoading={false} onItemSelect={mockOnItemSelect} />);
    expect(screen.getByText('No Data Found')).toBeInTheDocument();
  });
});

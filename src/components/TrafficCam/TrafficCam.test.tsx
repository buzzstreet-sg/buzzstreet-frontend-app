import React from 'react';
import { render, screen } from '@testing-library/react';
import TrafficCam from './TrafficCam';
import useFetchTrafficCam from '../../hooks/useFetchTrafficCam';

// Mocking the custom hook
jest.mock('../../hooks/useFetchTrafficCam');

describe('TrafficCam Component', () => {
  const selectedDate = new Date();
  const selectedLocation = 'Location 1';
  const mockImages = ['image1.jpg', 'image2.jpg', 'image3.jpg'];

  beforeEach(() => {
    // Clear all mocks before each test
    jest.clearAllMocks();
  });

  test('renders loading state', () => {
    (useFetchTrafficCam as jest.Mock).mockReturnValue({ isLoading: true, data: null, error: null });
    render(<TrafficCam selectedDate={selectedDate} selectedLocation={selectedLocation} />);
    expect(screen.getByText('Loading traffic camera data...')).toBeInTheDocument();
  });

  test('renders error state', () => {
    (useFetchTrafficCam as jest.Mock).mockReturnValue({ isLoading: false, data: null, error: 'Error message' });
    render(<TrafficCam selectedDate={selectedDate} selectedLocation={selectedLocation} />);
    expect(screen.getByText('Sorry, something went wrong. Please try again later.')).toBeInTheDocument();
  });

  test('renders no data state', () => {
    (useFetchTrafficCam as jest.Mock).mockReturnValue({ isLoading: false, data: { images: [] }, error: null });
    render(<TrafficCam selectedDate={selectedDate} selectedLocation={selectedLocation} />);
    expect(screen.getByText('No traffic cam data available.')).toBeInTheDocument();
  });

  test('renders images when data is available', () => {
    (useFetchTrafficCam as jest.Mock).mockReturnValue({ isLoading: false, data: { images: mockImages }, error: null });
    render(<TrafficCam selectedDate={selectedDate} selectedLocation={selectedLocation} />);

    const images = screen.getAllByAltText(`Traffic at ${selectedLocation}`);
    expect(images.length).toBe(mockImages.length);

    images.forEach((image, index) => {
      expect(image).toHaveAttribute('src', mockImages[index]);
    });
  });
});

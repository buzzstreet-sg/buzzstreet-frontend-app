/**
 * TrafficCam Component
 *
 * This component is designed to display traffic camera images based on a selected date and location.
 * It fetches traffic camera data from an API and renders the images in a scrollable container.
 * The component handles loading, error, and no data states, providing appropriate feedback or
 * placeholder content in each case. This component is ideal for applications dealing with real-time
 * or historical traffic data visualization.
 *
 * Props:
 * - selectedDate (Date | null): The date for which traffic camera data is to be fetched.
 * - selectedLocation (string): The location for which traffic camera data is to be fetched.
 *
 * Usage:
 * <TrafficCam
 *    selectedDate={selectedDate}
 *    selectedLocation={selectedLocation}
 * />
 */

import React, { useState, useEffect } from 'react';
import { Card, CardMedia, Box } from '@mui/material';
import './TrafficCam.css';
import useFetchTrafficCam from "../../hooks/useFetchTrafficCam";
import Placeholder from "../Placeholder/Placeholder";

interface TrafficCamProps {
  selectedDate: Date | null;
  selectedLocation: string;
}

const TrafficCam = ({ selectedDate, selectedLocation }: TrafficCamProps) => {
  const [images, setImages] = useState<string[]>([]);

  // API: Retrieve traffic cam data based on the selected datetime and location.
  const {
    data,
    isLoading,
    error
  } = useFetchTrafficCam(selectedDate, selectedLocation);

  // Store the list of images for display.
  useEffect(() => {
    setImages(data?.images || []);
  }, [data]);

  if (isLoading) {
    return <
        Placeholder
        message="Loading traffic camera data..."
        isLoading={true}
    />
  }

  if (!selectedDate || !selectedLocation) {
    return <
      Placeholder
        message="No date or location selected. Please select the date and location."
        icon="📷"
    />;
  }

  if (error) {
    return <
        Placeholder
        message="Sorry, something went wrong. Please try again later."
        icon="📷"
    />;
  }

  if (!images || images.length === 0) {
    return <
      Placeholder
        message="No traffic cam data available."
        icon="📷"
    />;
  }

  if (error) {
    return <
        Placeholder
        message="Sorry, something went wrong. Please try again later."
        icon="📷"
    />;
  }

  return (
      <Box className="trafficCamContainer">
        <div className="imageScrollContainer">
          {images.map((image, index) => (
              <Card key={index} className="imageCard">
                <CardMedia
                    component="img"
                    image={image}
                    alt={`Traffic at ${selectedLocation}`}
                />
              </Card>
          ))}
        </div>
      </Box>
  );
};

export default TrafficCam;

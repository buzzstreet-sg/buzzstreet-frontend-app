import React from 'react';
import { Route, Routes } from 'react-router-dom';
import { Container, AppBar, Toolbar, Typography, Box } from '@mui/material';
import HomePage from './pages/HomePage';

function App() {
  return (
      <div className="App">
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6">
              BuzzStreetSG
            </Typography>
          </Toolbar>
        </AppBar>

        <Container maxWidth="lg">
          <Box my={4}>
            <Routes>
              <Route path="/" element={<HomePage />} />
            </Routes>
          </Box>
        </Container>
      </div>
  );
}

export default App;

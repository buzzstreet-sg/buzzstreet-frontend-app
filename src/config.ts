const BS_API_HOST = process.env.BS_APP_API_URL || 'http://localhost:3001';

interface AppConfig {
  BS_API_HOST: string;
}

const appConfig: AppConfig = {
  BS_API_HOST,
};

export default appConfig;